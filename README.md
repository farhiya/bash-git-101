# Bash, Git, Bitbucket Intro (Markdown)

## Bash

Bash is a linux language to interact with the operationg system. We do this via the terminal.

### Main Bash commands

- `pwd` - print working directory - shows you where you are
- `cd` - change directory also ` cd ..` - goes back one folder
- `ls` - list short - shows folders and files in current directory
- `ll` - list long
- `ls -a` - shows folders and files in current directory including those that are hidden
- `mkdir` - creates a folder
- `touch` - creates a file
- `echo` - outputs a string
- `rm` - removes a file
- `rm -rf` - removes a folder
- `clear` - clears terminal
- `cat` - shows me what's in the file 

## Git

Git is version control.
This is where your work is stored. You can view all changed made and multiple team members can access/add/update the work that's on there.

### Main Git commands

#### Initializing and configuration

-  `gitinit`- start a git repo using

#### Saving

- `git add <file name>` - this will add the files save changes
- `git add .` - saves all in this directory

#### Staging (readying files to upload)

- `git commit -m "commit message"` - this will stage changes ( makes sure file/folder changes are ready to be pushed onto github/bitbucket repo)

#### Uploading to repo (push) 

- `git push` - sends staged local changes to the repo

#### Getting updated version of work

- `git pull` - grabs updated version from the 'global' repo to your 'local' repo (your computer) 

- `git status` - tells you if there are any unsaved/not commited files

## Bitbucket

Bitbucket is an online location for your code to live where you can collaborate with your collegues.

#### Add path to bitbucket
- `git remote add origin <orgin name> ` 

#### Upload to bitbucket
- `git push origin master` - uploads work to bitbucket
  
#### Do our checks 
- check bitbucket has the uploaded work
- check commit history to see edits
  


## Markdown

Markdown is what I'm writing right now.